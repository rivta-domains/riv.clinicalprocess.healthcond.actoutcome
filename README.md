* Instruktioner för hur man kommer igång med bitbucket http://rivta.se/bitbucket/git-sourcetree.html

# [För kompletterande information angående tjänstedomänen, till exempel release notes, stöddokumentation och anvisning för hur kod genereras] https://inera.atlassian.net/wiki/spaces/OITOF/overview?homepageId=21168243



OBS! För verifiering enligt testmodell 2.0 skall testsviter och självdeklarationer för domänversion 2.0.2 användas för kontraktet GetMaternityMedicalHistory 2.0.
Denna hittas [här](https://bitbucket.org/rivta-domains/riv.clinicalprocess.healthcond.actoutcome/src/478b7b4e12c0364155d778a12c9557a536ddfdd5?at=2.0.2)
